package com.electrolux.testtask.rest.model.dto.request;

import com.electrolux.testtask.domain.model.action.UserAction;
import com.electrolux.testtask.domain.model.entity.DeviceType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

import java.util.Map;
import java.util.UUID;

@Value
@Builder
@With
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserActionRequest {
	//TODO for auth
	private final UUID userId;
	private final String token;

	@NonNull
	private final DeviceType deviceType;
	@NonNull
	private final UUID deviceId;
	@NonNull
	private final String deviceSecureCode;
	@NonNull
	private final String actionName;
	private final Map<String, String> actionOptions;


	public UserAction toUserAction() {
		return UserAction.builder()
				.deviceId(deviceId)
				.deviceType(deviceType)
				.deviceSecureCode(deviceSecureCode)

				.actionName(actionName)
				.actionOptions(actionOptions)
				.build();
	}
}
