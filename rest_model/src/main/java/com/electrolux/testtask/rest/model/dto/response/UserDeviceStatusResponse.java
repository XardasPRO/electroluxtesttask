package com.electrolux.testtask.rest.model.dto.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Value;
import lombok.With;

import java.util.Map;

@Value
@Builder
@With
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserDeviceStatusResponse {
	private final String status;
	private final Map<String, String> statusOptions;
}
