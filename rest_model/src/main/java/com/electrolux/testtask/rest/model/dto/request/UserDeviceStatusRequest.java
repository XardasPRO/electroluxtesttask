package com.electrolux.testtask.rest.model.dto.request;

import com.electrolux.testtask.domain.model.entity.DeviceType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

import java.util.UUID;

@Value
@Builder
@With
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserDeviceStatusRequest {
	//TODO for auth
	private final UUID userId;
	private final String token;

	@NonNull
	private final DeviceType deviceType;
	@NonNull
	private final UUID deviceId;
}
