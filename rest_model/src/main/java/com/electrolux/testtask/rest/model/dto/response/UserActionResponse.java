package com.electrolux.testtask.rest.model.dto.response;

import com.electrolux.testtask.domain.model.entity.DeviceType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@Value
@Builder
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserActionResponse {
	@NonNull
	private final DeviceType deviceType;
	@NonNull
	private final UUID deviceId;
	@Builder.Default
	private final int errorCode = 0;
	private final String errorMessage;
}
