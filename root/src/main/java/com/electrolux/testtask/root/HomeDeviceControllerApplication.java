package com.electrolux.testtask.root;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.electrolux.testtask")
public class HomeDeviceControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeDeviceControllerApplication.class, args);
	}
}
