package com.electrolux.testtask.root.controller;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.device.StatesEnum;
import com.electrolux.testtask.domain.model.entity.DeviceType;
import com.electrolux.testtask.domain.model.entity.WashMachine;
import com.electrolux.testtask.domain.service.DeviceControlService;
import com.electrolux.testtask.rest.model.dto.request.CreateDeviceRequest;
import com.electrolux.testtask.rest.model.dto.request.UserActionRequest;
import com.electrolux.testtask.rest.model.dto.request.UserDeviceStatusRequest;
import com.electrolux.testtask.rest.model.dto.response.UserActionResponse;
import com.electrolux.testtask.rest.model.dto.response.UserDeviceStatusResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserActionsController {

	@Autowired
	private DeviceControlService controlService;

	@RequestMapping("/control/device/action")
	public @ResponseBody
	UserActionResponse sendCommand(@RequestBody UserActionRequest request) {

		try {
			controlService.processUserAction(request.toUserAction());
		} catch (ChangeStateException e) {
			e.printStackTrace();
			return UserActionResponse.builder()
					.errorMessage(e.getMessage())
					.deviceType(DeviceType.WASH_MACHINE)
					.build();
		}


		return UserActionResponse.builder()
				.deviceType(request.getDeviceType())
				.deviceId(request.getDeviceId())
				.errorCode(-1)
				.build();
	}

	@RequestMapping("/control/device/status")
	public @ResponseBody
	UserDeviceStatusResponse getStatus(@RequestBody UserDeviceStatusRequest request) {
		StatesEnum deviceStatus = controlService.getDeviceStatus(request.getDeviceType(), request.getDeviceId());
		return UserDeviceStatusResponse.builder()
				.status(deviceStatus.toString())
				.build();
	}

	@RequestMapping("/control/device/create")
	public @ResponseBody
	void createDevice(@RequestBody CreateDeviceRequest request) {
		if (DeviceType.WASH_MACHINE.equals(request.getDeviceType())) {

			WashMachine washMachine = new WashMachine(request.getDeviceId(), request.getDeviceSecureCode());

			controlService.createDevice(DeviceType.WASH_MACHINE, washMachine);
		}
	}
}
