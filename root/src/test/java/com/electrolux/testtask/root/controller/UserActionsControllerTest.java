package com.electrolux.testtask.root.controller;

import com.electrolux.testtask.domain.device.washmachine.WashMachineStates;
import com.electrolux.testtask.domain.model.entity.DeviceType;
import com.electrolux.testtask.rest.model.dto.request.CreateDeviceRequest;
import com.electrolux.testtask.rest.model.dto.request.UserActionRequest;
import com.electrolux.testtask.rest.model.dto.request.UserDeviceStatusRequest;
import com.electrolux.testtask.rest.model.dto.response.UserActionResponse;
import com.electrolux.testtask.rest.model.dto.response.UserDeviceStatusResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.net.URL;
import java.util.UUID;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserActionsControllerTest {
	@LocalServerPort
	private int port;

	private URL base;

	@Autowired
	private TestRestTemplate template;

	@BeforeEach
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + port + "/control/device");
	}

	@Test
	public void shouldCreateNewDevice() {
		UUID deviceId = UUID.randomUUID();
		ResponseEntity<String> response = template.postForEntity(base.toString() + "/create",
				CreateDeviceRequest.builder()
						.deviceId(deviceId)
						.deviceType(DeviceType.WASH_MACHINE)
						.deviceSecureCode("0987")
						.build(),
				String.class);

		ResponseEntity<UserDeviceStatusResponse> statusResponse = template.postForEntity(base.toString() + "/status",
				UserDeviceStatusRequest.builder()
						.deviceId(deviceId)
						.deviceType(DeviceType.WASH_MACHINE)
						.build(),
				UserDeviceStatusResponse.class);
		Assertions.assertEquals(WashMachineStates.READY, WashMachineStates.valueOf(statusResponse.getBody().getStatus()));
	}

	@Test
	public void shouldChangeDeviceState() {
		UUID deviceId = UUID.randomUUID();
		ResponseEntity<String> response = template.postForEntity(base.toString() + "/create",
				CreateDeviceRequest.builder()
						.deviceId(deviceId)
						.deviceType(DeviceType.WASH_MACHINE)
						.deviceSecureCode("0987")
						.build(),
				String.class);

		ResponseEntity<UserActionResponse> actionResponse = template.postForEntity(base.toString() + "/action",
				UserActionRequest.builder()
						.actionName(WashMachineStates.WORK.toString())
						.deviceId(deviceId)
						.deviceType(DeviceType.WASH_MACHINE)
						.deviceSecureCode("0987")
						.build(),
				UserActionResponse.class);
		Assertions.assertEquals(-1, actionResponse.getBody().getErrorCode());

		ResponseEntity<UserDeviceStatusResponse> statusResponse = template.postForEntity(base.toString() + "/status",
				UserDeviceStatusRequest.builder()
						.deviceId(deviceId)
						.deviceType(DeviceType.WASH_MACHINE)
						.build(),
				UserDeviceStatusResponse.class);
		Assertions.assertEquals(WashMachineStates.WORK, WashMachineStates.valueOf(statusResponse.getBody().getStatus()));
	}
}