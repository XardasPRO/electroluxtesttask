#ElectroluxTestTask

Microservice to emulate control on home devices. Support only WASH_MACHINE and dont have a storage.


#API

###Create a new Device 
Url: localhost:8080/control/device/create 

Type: POST

Json example:

`{
	"userId": "60d81314-e1ed-49d6-81b8-250ea5648753",
	"token": "some_access_token",
	"deviceType": "WASH_MACHINE",
	"deviceId": "60d81314-e1ed-49d6-81b8-250ea1000000",
	"deviceSecureCode": "1234",
}`

property | type | description | Is Working | Required
------------ | ------------- | ------------------- | ----------- | ---------
userId | UUID | User identification | fale | false
token | String | User authorisation | false | false
deviceType | String | Type of device (WASH_MACHINE) | true | true
deviceId | UUID | Device identification | true | true
deviceSecureCode | String | Code to share device between users| false | false

Response: void

###Get device status
Url: localhost:8080/control/device/status 

Type: POST

Json example:

`{
	"userId": "60d81314-e1ed-49d6-81b8-250ea5648753",
	"token": "some_access_token",
	"deviceType": "WASH_MACHINE",
	"deviceId": "60d81314-e1ed-49d6-81b8-250ea1000000",
	"deviceSecureCode": "1234",
}`

property | type | description | Is Working | Required
------------ | ------------- | ------------------- | ----------- | ---------
userId | UUID | User identification | fale | false
token | String | User authorisation | false | false
deviceType | String | Type of device (WASH_MACHINE) | true | true
deviceId | UUID | Device identification | true | true
deviceSecureCode | String | Code to share device between users| false | false

Response:

`{
      "status": "READY",
      "statusOptions": null
  }`
  
property | type | description 
------------ | ------------- | ------------------- 
status | String | Current device state
statusOptions | Map | Additional state options (not working)



###User action
Url: localhost:8080/control/device/action 

Type: POST

Json example:

`{
 	"userId": "60d81314-e1ed-49d6-81b8-250ea5648753",
 	"token": "some_access_token",
 	"deviceType": "WASH_MACHINE",
 	"deviceId": "60d81314-e1ed-49d6-81b8-250ea1000000",
 	"deviceSecureCode": "1234",
 	"actionName": "PAUSE",
 	"actionOptions" : {
 		"startTime": "12:25"
 	}
 }`

property | type | description | Is Working | Required
------------ | ------------- | ------------------- | ----------- | ---------
userId | UUID | User identification | false | false
token | String | User authorisation | false | false
deviceType | String | Type of device (WASH_MACHINE) | true | true
deviceId | UUID | Device identification | true | true
deviceSecureCode | String | Code to share device between users| false | false
actionName | String | Target device action (for wash machine: READY, WORK, PAUSE, DONE) | true | true
actionOptions | Map | Additional action options (like time of start working) | false | false


Response:

`{
      "status": "READY",
      "statusOptions": null
  }`
  
property | type | description 
------------ | ------------- | ------------------- 
status | String | Current device state
statusOptions | Map | Additional state options (not working)