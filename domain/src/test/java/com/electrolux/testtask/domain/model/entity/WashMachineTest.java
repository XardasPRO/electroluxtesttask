package com.electrolux.testtask.domain.model.entity;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.device.washmachine.WashMachineStates;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.UUID;
import java.util.stream.Stream;

class WashMachineTest {
	WashMachine testee = new WashMachine(UUID.randomUUID(), "12345");

	private static Stream<Arguments> invalidStatesProvider() {
		return Stream.of(
				Arguments.of(WashMachineStates.READY, WashMachineStates.PAUSE),
				Arguments.of(WashMachineStates.READY, WashMachineStates.DONE),
				Arguments.of(WashMachineStates.WORK, WashMachineStates.READY),
				Arguments.of(WashMachineStates.PAUSE, WashMachineStates.READY),
				Arguments.of(WashMachineStates.PAUSE, WashMachineStates.DONE),
				Arguments.of(WashMachineStates.DONE, WashMachineStates.WORK),
				Arguments.of(WashMachineStates.DONE, WashMachineStates.PAUSE)
		);
	}

	private static Stream<Arguments> validStatesProvider() {
		return Stream.of(
				Arguments.of(WashMachineStates.READY, WashMachineStates.WORK),
				Arguments.of(WashMachineStates.WORK, WashMachineStates.PAUSE),
				Arguments.of(WashMachineStates.WORK, WashMachineStates.DONE),
				Arguments.of(WashMachineStates.PAUSE, WashMachineStates.WORK),
				Arguments.of(WashMachineStates.DONE, WashMachineStates.READY)
		);
	}

	@ParameterizedTest
	@MethodSource("invalidStatesProvider")
	void shouldThrowExceptionOnIncorrectStateTransfer(WashMachineStates currentState, WashMachineStates targetState) throws ChangeStateException {
		testee.setState(currentState);
		Assertions.assertThrows(ChangeStateException.class, () ->
				testee.applyState(targetState)
		);
	}

	@ParameterizedTest
	@MethodSource("validStatesProvider")
	void shouldPassOnCorrectStateTransfer(WashMachineStates currentState, WashMachineStates targetState) throws ChangeStateException {
		testee.setState(currentState);
		testee.applyState(targetState);
	}

	@Test
	void shouldPassFirstSuccessScenario() throws ChangeStateException {
		testee.applyState(WashMachineStates.WORK);
		testee.applyState(WashMachineStates.PAUSE);
		testee.applyState(WashMachineStates.WORK);
		testee.applyState(WashMachineStates.DONE);
		testee.applyState(WashMachineStates.READY);
	}

	@Test
	void shouldPassSecondSuccessScenario() throws ChangeStateException {
		testee.applyState(WashMachineStates.WORK);
		testee.applyState(WashMachineStates.DONE);
		testee.applyState(WashMachineStates.READY);
	}

	@Test
	void shouldPassThirdSuccessScenario() throws ChangeStateException {
		testee.applyState(WashMachineStates.WORK);
		testee.applyState(WashMachineStates.PAUSE);
		testee.applyState(WashMachineStates.WORK);
		testee.applyState(WashMachineStates.PAUSE);
		testee.applyState(WashMachineStates.WORK);
		testee.applyState(WashMachineStates.PAUSE);
		testee.applyState(WashMachineStates.WORK);
		testee.applyState(WashMachineStates.PAUSE);
		testee.applyState(WashMachineStates.WORK);
		testee.applyState(WashMachineStates.DONE);
		testee.applyState(WashMachineStates.READY);
	}

}