package com.electrolux.testtask.domain.device.washmachine;

import com.electrolux.testtask.domain.command.DeviceCommand;
import com.electrolux.testtask.domain.command.washmachine.DoneWashMachineCommand;
import com.electrolux.testtask.domain.command.washmachine.PauseWashMachineCommand;
import com.electrolux.testtask.domain.command.washmachine.ReadyWashMachineCommand;
import com.electrolux.testtask.domain.command.washmachine.WorkWashMachineCommand;
import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.device.StatesEnum;
import com.electrolux.testtask.domain.model.entity.WashMachine;

public enum WashMachineStates implements StatesEnum<WashMachine> {
	READY(0, new ReadyWashMachineCommand()),
	WORK(1, new WorkWashMachineCommand()),
	PAUSE(2, new PauseWashMachineCommand()),
	DONE(3, new DoneWashMachineCommand());

	private int id;
	private DeviceCommand command;

	WashMachineStates(int id, DeviceCommand deviceCommand) {
		this.id = id;
		this.command = deviceCommand;
	}

	public int getId() {
		return id;
	}

	@Override
	public void applyState(WashMachine device) throws ChangeStateException {
		command.execute(device);
	}
}
