package com.electrolux.testtask.domain.device.washmachine;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.device.State;
import com.electrolux.testtask.domain.model.entity.WashMachine;

public abstract class WashMachineState implements State {
	protected WashMachine washMachine;
	protected WashMachineStates state;

	public WashMachineState(WashMachine washMachine) {
		this.washMachine = washMachine;
	}

	@Override
	public int getStateId() {
		return state.getId();
	}

	public abstract void onReady() throws ChangeStateException;
	public abstract void onWork() throws ChangeStateException;
	public abstract void onPause() throws ChangeStateException;
	public abstract void onDone() throws ChangeStateException;
}
