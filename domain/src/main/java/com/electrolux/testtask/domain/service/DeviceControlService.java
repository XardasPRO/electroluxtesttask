package com.electrolux.testtask.domain.service;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.device.StatesEnum;
import com.electrolux.testtask.domain.device.washmachine.WashMachineStates;
import com.electrolux.testtask.domain.model.action.UserAction;
import com.electrolux.testtask.domain.model.entity.Device;
import com.electrolux.testtask.domain.model.entity.DeviceType;
import com.electrolux.testtask.domain.model.entity.WashMachine;
import com.electrolux.testtask.domain.storage.WashMachineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;

@Service
public class DeviceControlService implements UserActionService {

	@Autowired
	private WashMachineRepository washMachineRepository;

	@Override
	public void processUserAction(UserAction userAction) throws ChangeStateException {
		if (userAction.getDeviceType().equals(DeviceType.WASH_MACHINE)) {
			WashMachine device = washMachineRepository.getDeviceById(userAction.getDeviceId());
			if (device == null) {
				throw new ChangeStateException("Device is not exist");
			}

			WashMachineStates washMachineState = getWashMachineState(userAction.getActionName(), userAction.getActionOptions());
			if (washMachineState == null) {
				throw new ChangeStateException("Unsupported device state");
			}

			washMachineState.applyState(device);
		} else {
			throw new ChangeStateException("Unsupported device type");
		}
	}

	@Override
	public StatesEnum getDeviceStatus(DeviceType type, UUID deviceId) {
		if (DeviceType.WASH_MACHINE.equals(type)) {
			WashMachine device = washMachineRepository.getDeviceById(deviceId);
			if (device != null) {
				for (WashMachineStates value : WashMachineStates.values()) {
					if (value.getId() == device.getState().getStateId()) {
						return value;
					}
				}
			}
		}
		return null;
	}

	@Override
	public void createDevice(DeviceType type, Device device) {
		if (DeviceType.WASH_MACHINE.equals(type)) {
			washMachineRepository.updateDevice((WashMachine) device);
		}
	}

	private WashMachineStates getWashMachineState(String stateName, Map<String, String> options) {
		return WashMachineStates.valueOf(stateName);
	}
}
