package com.electrolux.testtask.domain.device.washmachine;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.model.entity.WashMachine;

public class WashMachineWorkState extends WashMachineState {
	public WashMachineWorkState(WashMachine washMachine) {
		super(washMachine);
		this.state = WashMachineStates.WORK;
	}

	@Override
	public void onReady() throws ChangeStateException {
		throw new ChangeStateException("Next state should be \"Pause\" or \"Done\"");
	}

	@Override
	public void onWork() throws ChangeStateException {
		throw new ChangeStateException("Next state should be \"Pause\" or \"Done\"");
	}

	@Override
	public void onPause() throws ChangeStateException {
		washMachine.setState(WashMachineStates.PAUSE);
	}

	@Override
	public void onDone() throws ChangeStateException {
		washMachine.setState(WashMachineStates.DONE);
	}
}
