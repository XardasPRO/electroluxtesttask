package com.electrolux.testtask.domain.storage;

import com.electrolux.testtask.domain.model.entity.Device;

import java.util.UUID;

public interface DevicesRepository<T extends Device> {
	T getDeviceById(UUID deviceId);

	void updateDevice(T device);
}
