package com.electrolux.testtask.domain.device.washmachine;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.model.entity.WashMachine;

public class WashMachineReadyState extends WashMachineState {
	public WashMachineReadyState(WashMachine washMachine) {
		super(washMachine);
		this.state = WashMachineStates.READY;
	}

	@Override
	public void onReady() throws ChangeStateException {
		throw new ChangeStateException("Next state should be \"Work\"");
	}

	@Override
	public void onWork() throws ChangeStateException {
		washMachine.setState(WashMachineStates.WORK);
	}

	@Override
	public void onPause() throws ChangeStateException {
		throw new ChangeStateException("Next state should be \"Work\"");
	}

	@Override
	public void onDone() throws ChangeStateException {
		throw new ChangeStateException("Next state should be \"Work\"");
	}
}
