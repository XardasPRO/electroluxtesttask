package com.electrolux.testtask.domain.service;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.device.StatesEnum;
import com.electrolux.testtask.domain.model.action.UserAction;
import com.electrolux.testtask.domain.model.entity.Device;
import com.electrolux.testtask.domain.model.entity.DeviceType;

import java.util.UUID;

public interface UserActionService {
	void processUserAction(UserAction userAction) throws ChangeStateException;
	StatesEnum getDeviceStatus(DeviceType type, UUID deviceId);
	void createDevice(DeviceType type, Device device);
}
