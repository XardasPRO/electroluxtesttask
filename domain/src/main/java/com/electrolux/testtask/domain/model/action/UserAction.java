package com.electrolux.testtask.domain.model.action;

import com.electrolux.testtask.domain.model.entity.DeviceType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.Map;
import java.util.UUID;

@Value
@Builder
@AllArgsConstructor(staticName = "of")
public class UserAction {
	@NonNull
	private final DeviceType deviceType;
	@NonNull
	private final UUID deviceId;
	@NonNull
	private final String deviceSecureCode;


	@NonNull
	private final String actionName;
	private final Map<String, String> actionOptions;
}
