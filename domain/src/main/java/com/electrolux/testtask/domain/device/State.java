package com.electrolux.testtask.domain.device;

public interface State {
	int getStateId();
}
