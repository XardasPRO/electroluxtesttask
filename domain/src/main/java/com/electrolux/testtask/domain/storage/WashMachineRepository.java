package com.electrolux.testtask.domain.storage;

import com.electrolux.testtask.domain.model.entity.WashMachine;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class WashMachineRepository implements DevicesRepository<WashMachine> {
	private static final Map<UUID, WashMachine> storage = new ConcurrentHashMap<>();

	@Override
	public WashMachine getDeviceById(UUID deviceId) {
		return storage.get(deviceId);
	}

	@Override
	public void updateDevice(WashMachine device) {
		storage.put(device.getId(), device);
	}
}
