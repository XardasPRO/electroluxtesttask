package com.electrolux.testtask.domain.model.entity;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.device.washmachine.WashMachineDoneState;
import com.electrolux.testtask.domain.device.washmachine.WashMachinePauseState;
import com.electrolux.testtask.domain.device.washmachine.WashMachineReadyState;
import com.electrolux.testtask.domain.device.washmachine.WashMachineState;
import com.electrolux.testtask.domain.device.washmachine.WashMachineStates;
import com.electrolux.testtask.domain.device.washmachine.WashMachineWorkState;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class WashMachine implements Device<WashMachineState, WashMachineStates> {
	private static final DeviceType type = DeviceType.WASH_MACHINE;
	@NonNull
	private final UUID id;
	@NonNull
	private String secureCode;
	@NonNull
	private WashMachineState state;

	public WashMachine(@NonNull UUID id, String secureCode) {
		this.id = id;
		this.secureCode = secureCode;
		this.state = new WashMachineReadyState(this);
	}

	public void setState(WashMachineStates state) throws ChangeStateException {
		switch (state) {
			case READY:
				this.state = new WashMachineReadyState(this);
				break;
			case WORK:
				this.state = new WashMachineWorkState(this);
				break;
			case PAUSE:
				this.state = new WashMachinePauseState(this);
				break;
			case DONE:
				this.state = new WashMachineDoneState(this);
				break;
			default:
				throw new ChangeStateException("Unsupported state request");
		}
	}

	@Override
	public void applyState(WashMachineStates nextState) throws ChangeStateException {
		switch (nextState) {
			case READY:
				state.onReady();
				break;
			case WORK:
				state.onWork();
				break;
			case PAUSE:
				state.onPause();
				break;
			case DONE:
				state.onDone();
				break;
			default:
				throw new ChangeStateException("Unsupported state request");
		}
	}

	@Override
	public WashMachineState getCurrentState() {
		return state;
	}
}
