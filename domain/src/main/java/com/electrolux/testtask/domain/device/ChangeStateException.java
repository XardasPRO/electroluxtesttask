package com.electrolux.testtask.domain.device;

public class ChangeStateException extends Exception {
	public ChangeStateException(String message) {
		super(message);
	}

	public ChangeStateException(String message, Throwable cause) {
		super(message, cause);
	}
}
