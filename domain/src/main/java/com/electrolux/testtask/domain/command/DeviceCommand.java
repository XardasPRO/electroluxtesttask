package com.electrolux.testtask.domain.command;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.model.entity.Device;

public interface DeviceCommand<T extends Device>  {
	void execute(T device) throws ChangeStateException;
}
