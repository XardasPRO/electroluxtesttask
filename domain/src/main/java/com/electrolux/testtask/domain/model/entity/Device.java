package com.electrolux.testtask.domain.model.entity;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.device.State;
import com.electrolux.testtask.domain.device.StatesEnum;

public interface Device<T extends State, R extends StatesEnum> {
	void applyState(R state) throws ChangeStateException;
	T getCurrentState();
}
