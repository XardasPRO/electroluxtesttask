package com.electrolux.testtask.domain.device;

import com.electrolux.testtask.domain.model.entity.Device;

public interface StatesEnum<T extends Device> {
	void applyState(T device) throws ChangeStateException;
}
