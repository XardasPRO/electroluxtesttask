package com.electrolux.testtask.domain.device.washmachine;

import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.model.entity.WashMachine;

public class WashMachineDoneState extends WashMachineState {
	public WashMachineDoneState(WashMachine washMachine) {
		super(washMachine);
		this.state = WashMachineStates.DONE;
	}

	@Override
	public void onReady() throws ChangeStateException {
		washMachine.setState(WashMachineStates.READY);
	}

	@Override
	public void onWork() throws ChangeStateException {
		throw new ChangeStateException("Next state should be \"Ready\"");
	}

	@Override
	public void onPause() throws ChangeStateException {
		throw new ChangeStateException("Next state should be \"Ready\"");
	}

	@Override
	public void onDone() throws ChangeStateException {
		throw new ChangeStateException("Next state should be \"Ready\"");
	}
}
