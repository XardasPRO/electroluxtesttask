package com.electrolux.testtask.domain.model.entity;

public enum DeviceType {
	WASH_MACHINE,
	MICROWAVE
}
