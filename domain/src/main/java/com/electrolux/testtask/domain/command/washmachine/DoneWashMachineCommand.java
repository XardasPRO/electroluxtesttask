package com.electrolux.testtask.domain.command.washmachine;

import com.electrolux.testtask.domain.command.DeviceCommand;
import com.electrolux.testtask.domain.device.ChangeStateException;
import com.electrolux.testtask.domain.device.washmachine.WashMachineStates;
import com.electrolux.testtask.domain.model.entity.WashMachine;

public class DoneWashMachineCommand implements DeviceCommand<WashMachine> {
	@Override
	public void execute(WashMachine device) throws ChangeStateException {
		device.applyState(WashMachineStates.DONE);
	}
}
